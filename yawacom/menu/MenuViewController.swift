//
//  MenuViewController.swift
//  yawacom
//
//  Created by 久野文菜 on 2021/03/27.
//

import Foundation
import RxSwift

final class MenuViewController: UIViewController {
    
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var recordViewButton: UIButton!
    @IBOutlet weak var rankingButton: UIButton!
    
    //    private let menuView = MenuView()
    private var viewModel: MenuViewModel!
    
    static func start(
        _ currentVC: UIViewController,
        animated: Bool = false
    ) -> MenuViewController {
        let nextVC = MenuViewController(nibName: "MenuView", bundle: nil)   // .initは省略可能
        currentVC.navigationController?.pushViewController(nextVC, animated: animated)
        // 上のナビゲーションバーを非表示
        currentVC.navigationController?.navigationBar.isHidden = true
        return nextVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewModel = MenuViewModel()
        
        let input = createInput()
        let output = viewModel.transform(input: input)
        setupOutput(output)
    }
    
    private let bag = DisposeBag()
    
    private func createInput() -> MenuViewModel.Input {
        return MenuViewModel.Input(
            onTapRecordButton: self.recordButton.rx.tap.asSignal(),
            onTapRecordViewButton: self.recordViewButton.rx.tap.asSignal(),
            onTapRankingButton: self.rankingButton.rx.tap.asSignal()
        )
    }
    
    private func setupOutput(_ output: MenuViewModel.Output) {
        output.showRecordView
            .drive(onNext: { [unowned self] in
                RecordViewController.start(self)
            }).disposed(by: bag)
        output.showRecordViewView
            .drive(onNext: { [unowned self] in
                RecordViewViewController.start(self)
            }).disposed(by: bag)
        output.showRankingView
            .drive(onNext: { [unowned self] in
                RankingViewController.start(self)
            }).disposed(by: bag)
    }
}
