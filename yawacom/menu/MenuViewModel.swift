//
//  MenuViewModel.swift
//  yawacom
//
//  Created by 久野文菜 on 2021/03/27.
//

import Foundation
import RxSwift
import RxCocoa

class MenuViewModel: ViewModelType {
    private let bag = DisposeBag()
    
    func transform(input: Input) -> Output {
        let showRecordView = PublishRelay<Void>()
        let showRecordViewView = PublishRelay<Void>()
        let showRankingView = PublishRelay<Void>()
        
        input.onTapRecordButton
            .emit(onNext: {
                showRecordView.accept(())
                print("✏️押された")
            })
            .disposed(by: self.bag)
        input.onTapRecordViewButton
            .emit(onNext: {
                showRecordViewView.accept(())
                print("🩰押された")
            })
            .disposed(by: self.bag)
        input.onTapRankingButton
            .emit(onNext: {
                showRankingView.accept(())
                print("👑押された")
            })
            .disposed(by: self.bag)
        
        return MenuViewModel.Output(
            showRecordView: showRecordView.asDriverOnErrorJustComplete(),
            showRecordViewView: showRecordViewView.asDriverOnErrorJustComplete(),
            showRankingView: showRankingView.asDriverOnErrorJustComplete()
        )
    }

    struct Input {
        let onTapRecordButton: Signal<Void>
        let onTapRecordViewButton: Signal<Void>
        let onTapRankingButton: Signal<Void>
    }

    struct Output {
        let showRecordView: Driver<Void>
        let showRecordViewView: Driver<Void>
        let showRankingView: Driver<Void>
    }

}

