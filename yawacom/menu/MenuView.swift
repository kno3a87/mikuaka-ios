//
//  MenuView.swift
//  yawacom
//
//  Created by 久野文菜 on 2021/03/27.
//

import UIKit

final class MenuView: UIView {

    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var recordViewButton: UIButton!
    @IBOutlet weak var rankingButton: UIButton!

    init() {
        super.init(frame: .zero)

        let view = UINib(nibName: "MenuView", bundle: nil).instantiate(withOwner: self, options: nil).first as! UIView
        // メニュータブの大きさ見てみる
//        print("MenuViewY:", UIScreen.main.bounds.height-view.bounds.height)
//        print("MenuViewWidth:", view.bounds.size.width)
//        print("MenuViewHight:", view.bounds.size.height)
        addSubview(view)
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
}

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
