//
//  UIViewExt.swift
//  yawacom
//
//  Created by 久野文菜 on 2021/04/04.
//

import UIKit

extension UIView {
    func addAndFit(subview: UIView) {
        // Autosizingからの変換を無効にする
        subview.translatesAutoresizingMaskIntoConstraints = false

        self.addSubview(subview)

        NSLayoutConstraint.activate([
            subview.leftAnchor.constraint(equalTo: leftAnchor),
            subview.rightAnchor.constraint(equalTo: rightAnchor),
            subview.topAnchor.constraint(equalTo: topAnchor),
            subview.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
}
