//
//  ApiPath.swift
//  yawacom
//
//  Created by 久野文菜 on 2021/03/28.
//

import Foundation

// シミュレータはlocalhost
// 実機はPCのIPアドレス（192.168.10.102）
//let Domain = "http://192.168.10.102:8080"
// herokuのURL
let Domain = "https://pacific-scrubland-42624.herokuapp.com"

public protocol TargetType {
    var domain: String { get }
    var path: String { get }
    var method: String { get }
}

// 実行されるAPIの種類を管理
public enum API {
    case login
    case register
    case record
    case recordView
    case rankingView
}

extension API: TargetType {
    public var domain : String {
        return Domain
    }

    public var path : String {
        switch self {
        // ログイン
        case .login:
            return "\(domain)" + "/user/login"
        // 新規登録
        case .register:
            return "\(domain)" + "/user"
        // やわらかさ登録
        case .record:
            return "\(domain)" + "/yawarakasa"
        // やわらかさ見る
        case .recordView:
            return "\(domain)" + "/yawarakasa"
        case .rankingView:
            return "\(domain)" + "/yawarakasa/ranking"
        }
    }

    public var method: String {
        switch self {
        case .login:
            return "POST"
        case .register:
            return "POST"
        case .record:
            return "POST"
        case .recordView:
            return "GET"
        case .rankingView:
            return "GET"
        }
    }
}
