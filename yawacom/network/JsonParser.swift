//
//  JsonParser.swift
//  yawacom
//
//  Created by 久野文菜 on 2021/03/28.
//

import UIKit
import SwiftyJSON

class JsonParser: NSObject {
    class var sharedInstance: JsonParser {
        struct Static {
            static let instance: JsonParser = JsonParser()
        }
        return Static.instance
    }

    // 叩くAPIによってparseするタイプを出しわけ
    func parseJson(_ api: API, json: JSON) -> Any? {
        switch api {
        case .profile, .register:
            return self.parseProfileApiJson(json)
        }
    }

    // プロフィール画面ユーザー情報取得
    private func parseProfileApiJson(_ json: JSON) -> UserModel {
        let userModel = UserModel()
        userModel.userName = json["name"].string
        userModel.password = json["password"].string

        return userModel
    }
}
