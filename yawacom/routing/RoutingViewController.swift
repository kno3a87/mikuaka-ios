//
//  RoutingViewController.swift
//  yawacom
//
//  Created by 久野文菜 on 2021/01/26.
//

import Foundation
import RxSwift

final class RoutingViewController: UIViewController {
    
    override func viewDidLoad() {
        print("ルーティングビューコントローラー")
        self.view.backgroundColor = .orange
        // UserDefaultsにトークンがあったらやわらかさ記録画面にいくように
        if let token = UserDefaults.standard.url(forKey: "access_token") {
//            print(token)  // デバッグ用
//            self.openLaunchScreen(launchScrenType: .record)
            // デバッグのためいったんログイン行くように
            self.openLaunchScreen(launchScrenType: .login)
        } else {
            self.openLaunchScreen(launchScrenType: .login)
        }
    }
}

extension RoutingViewController {
    private func openLaunchScreen(launchScrenType: LaunchScreenType) {
        
        switch launchScrenType {
        case .login:
            // ログイン画面へ移動
            LoginViewController.start(self)
        case .record:
            // やわらかさ記録画面へ移動
            RecordViewController.start(self)
            break
        }
    }
}


