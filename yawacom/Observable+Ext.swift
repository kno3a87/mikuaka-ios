//
//  Observable+Ext.swift
//  yawacom
//
//  Created by 久野文菜 on 2021/02/26.
//

import RxCocoa
import RxSwift

extension ObservableType {
    func asDriverOnErrorJustComplete() -> Driver<Element> {
        return asDriver { _ in
            Driver.empty()
        }
    }

    func asSignalOnErrorJustComplete() -> Signal<Element> {
        return asSignal { _ in
            Signal.empty()
        }
    }
}
