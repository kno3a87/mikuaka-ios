//
//  RankingViewController.swift
//  yawacom
//
//  Created by 久野文菜 on 2021/03/27.
//

import Foundation
import RxSwift
import SVProgressHUD

final class RankingViewController: UIViewController {
    @IBOutlet weak var userName1: UILabel!
    @IBOutlet weak var userYawarakasa1: UILabel!
    @IBOutlet weak var userName2: UILabel!
    @IBOutlet weak var userYawarakasa2: UILabel!
    
    static func start(
        _ currentVC: UIViewController,
        animated: Bool = false
    ) {
        let nextVC = RankingViewController(nibName: "RankingView", bundle: nil)   // .initは省略可能
        currentVC.navigationController?.pushViewController(nextVC, animated: animated)
        // 上のナビゲーションバーを非表示
        currentVC.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("ランキングビューコントローラー")
        
        self.createMenuVC()
        self.createHeaderVC()
        self.rankingView()
    }
    
    private func createMenuVC() {
        let menuVC = MenuViewController.start(self)
        addChild(menuVC)
        menuVC.didMove(toParent: self)
        menuVC.view.frame = CGRect(x: 0, y: view.bounds.size.height-120,
                                   width: view.bounds.size.width, height: 120)
        self.view.addSubview(menuVC.view)
        menuVC.rankingButton.backgroundColor = UIColor(named: "selectedMenuTab")
    }
    
    private func createHeaderVC() {
        let headerVC = HeaderViewController.start(self)
        addChild(headerVC)
        headerVC.didMove(toParent: self)
        headerVC.view.frame = CGRect(x: 0, y: 0,
                                   width: view.bounds.size.width, height: 150)
        self.view.addSubview(headerVC.view)
        headerVC.view.backgroundColor = UIColor(named: "header")
        headerVC.headerText.text = "ランキング"
        headerVC.headerText.font = .boldSystemFont(ofSize: 20)
        headerVC.headerText.textColor = .white
    }
}

// MARK: - Network
extension RankingViewController {
    private func rankingView() {
        let network = NetworkLayer()

        network.setStartHandler {()->() in
            SVProgressHUD.show(withStatus: "Loading...")
        }
        network.setErrorHandler {(error: NSError)->() in
            // 通信失敗時
            SVProgressHUD.dismiss()
            print("通信に失敗しました")
        }
        network.setFinishHandler {(result: Any?)->() in
            SVProgressHUD.dismiss()
        }
        
        // API実行
        network.requestApi(api: .rankingView,
                           parameters: nil ,
                           headers: nil,
                           completion: {(json) -> Void in
                            self.userName1.text = json[0]["user_name"].stringValue
                            self.userYawarakasa1.text = json[0]["yawarakasa"].stringValue
                            self.userName2.text = json[1]["user_name"].stringValue
                            self.userYawarakasa2.text = json[1]["yawarakasa"].stringValue
                            print(json[0]["user_name"].stringValue)
                           })
    }
}
