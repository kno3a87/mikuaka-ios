//
//  RecordViewViewController.swift
//  yawacom
//
//  Created by 久野文菜 on 2021/03/27.
//

import Foundation
import RxSwift
import Charts
import SVProgressHUD
import SwiftyJSON

final class RecordViewViewController: UIViewController {
    @IBOutlet weak var linechart: LineChartView!
    
    static func start(
        _ currentVC: UIViewController,
        animated: Bool = false
    ) {
        let nextVC = RecordViewViewController(nibName: "RecordViewView", bundle: nil)   // .initは省略可能
        currentVC.navigationController?.pushViewController(nextVC, animated: animated)
        // 上のナビゲーションバーを非表示
        currentVC.navigationController?.navigationBar.isHidden = true
    }
            
    override func viewDidLoad() {
        super.viewDidLoad()
        print("レコードビュービューコントローラー")
        
        self.createMenuVC()
        self.createHeaderVC()
        self.recordView()
    }
    
    private func setLineGraph(yawarakasa: [Int], date: [Double]){
        var yawarakasaEntries: [ChartDataEntry] = []
        
        // X軸のラベルの位置を下に設定
        linechart.xAxis.labelPosition = .bottom
        // X軸のラベルの色を設定
        linechart.xAxis.labelTextColor = .systemGray
        // X軸の線、グリッドを非表示にする
        linechart.xAxis.drawGridLinesEnabled = false
        linechart.xAxis.drawAxisLineEnabled = false
        // 右側のY座標軸は非表示にする
        linechart.rightAxis.enabled = false
        // Y座標の値が20始まりになるように設定
        linechart.leftAxis.axisMinimum = 20.0
        linechart.leftAxis.drawZeroLineEnabled = true
        linechart.leftAxis.zeroLineColor = .systemGray
        // ラベルの数を設定
        linechart.leftAxis.labelCount = 5
        // ラベルの色を設定
        linechart.leftAxis.labelTextColor = .systemGray
        // グリッドの色を設定
        linechart.leftAxis.gridColor = .systemGray
        // 軸線は非表示にする
        linechart.leftAxis.drawAxisLineEnabled = false
        
        for i in 0..<yawarakasa.count {
            yawarakasaEntries.append(ChartDataEntry(
                                        x: date[i],
                                        y: Double(yawarakasa[i])))
        }
        let dataset = LineChartDataSet(entries: yawarakasaEntries, label: "やわらかさ")
        dataset.circleRadius = 5
        linechart.data = LineChartData(dataSet: dataset)
    }
    
    private func createMenuVC() {
        let menuVC = MenuViewController.start(self)
        addChild(menuVC)
        menuVC.didMove(toParent: self)
        menuVC.view.frame = CGRect(x: 0, y: view.bounds.size.height-120,
                                   width: view.bounds.size.width, height: 120)
        self.view.addSubview(menuVC.view)
        menuVC.recordViewButton.backgroundColor = UIColor(named: "selectedMenuTab")
    }
    
    private func createHeaderVC() {
        let headerVC = HeaderViewController.start(self)
        addChild(headerVC)
        headerVC.didMove(toParent: self)
        headerVC.view.frame = CGRect(x: 0, y: 0,
                                   width: view.bounds.size.width, height: 150)
        self.view.addSubview(headerVC.view)
        headerVC.view.backgroundColor = UIColor(named: "header")
        headerVC.headerText.text = "kuno さんのやわらかさ"
        headerVC.headerText.font = .boldSystemFont(ofSize: 20)
        headerVC.headerText.textColor = .white
    }
}

// MARK: - Network
extension RecordViewViewController {
    
    private func recordView() {
        let network = NetworkLayer()

        network.setStartHandler {()->() in
            SVProgressHUD.show(withStatus: "Loading...")
        }
        network.setErrorHandler {(error: NSError)->() in
            // 通信失敗時
            SVProgressHUD.dismiss()
            print("通信に失敗しました")
        }
        network.setFinishHandler {(result: Any?)->() in
            SVProgressHUD.dismiss()
        }
        
        // API実行
        network.requestApi(api: .recordView,
                           parameters: nil ,
                           headers: ["x-access-token" : UserDefaults.standard.string(forKey: "access_token")!],
                           completion: {(json) -> Void in
                                print("----------------------------------------")
                                print(json[0])  // 何番めか指定できる
                                print(json.count)   // jsonの数
                                print("----------------------------------------")
                                print(json[0]["yawarakasa"])    // 指定要素目のやわらかさ
                                print("----------------------------------------")
                                print(type(of: json[0]["yawarakasa"]))  // 数字のみでもJSON型
                                print(type(of: json[0]["yawarakasa"].int))  // Optional<Int>になる（これ使えない）
                                print(json[0]["yawarakasa"].intValue)
                                print(type(of: json[0]["yawarakasa"].intValue))   // Intになる！！！！
                                print("----------------------------------------")
                                var yawarakasa: [Int] = []
                                for i in 0..<json.count {
                                    yawarakasa.append(json[i]["yawarakasa"].intValue)
                                }
                                print(yawarakasa)
                            
                                var year: [String] = []
                                for i in 0..<json.count {
                                    year.append(json[i]["year"].stringValue)
                                }
                                var month: [String] = []
                                for i in 0..<json.count {
                                    month.append(json[i]["month"].stringValue)
                                }
                                var day: [String] = []
                                for i in 0..<json.count {
                                    day.append(json[i]["day"].stringValue)
                                }
                            
                                var date: String
                                var doubleDate: [Double] = []
                                for i in 0..<year.count {
                                    date = year[i] + month[i] + day[i]
                                    doubleDate.append(Double(date)!)
                                }

                                // チャート表示
                                self.setLineGraph(yawarakasa: yawarakasa, date: doubleDate)
                           })
    }
}
