//
//  LaunchScreenType.swift
//  yawacom
//
//  Created by 久野文菜 on 2021/01/26.
//

import Foundation

enum LaunchScreenType: CaseIterable {
    case login
    case record
}
