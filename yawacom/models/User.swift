//
//  User.swift
//  yawacom
//
//  Created by 久野文菜 on 2021/01/29.
//

import Foundation


enum User: CaseIterable {
    case userName
    case password
}
