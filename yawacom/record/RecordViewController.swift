//
//  RecordViewController.swift
//  yawacom
//
//  Created by 久野文菜 on 2021/03/25.
//

import Foundation
import RxSwift
import SVProgressHUD

final class RecordViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var date: UITextField!
    @IBOutlet weak var yawarakasa: UITextField!
    @IBOutlet weak var recordButton: UIButton!
    
    private var viewModel: RecordViewModel!
    
    static func start(
        _ currentVC: UIViewController,
        animated: Bool = false
    ) {
        let nextVC = RecordViewController(nibName: "RecordView", bundle: nil)   // .initは省略可能
        currentVC.navigationController?.pushViewController(nextVC, animated: animated)
        // 上のナビゲーションバーを非表示
        currentVC.navigationController?.navigationBar.isHidden = true
    }
    
    private let bag = DisposeBag()
    private let datePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewModel = RecordViewModel()
        
        self.view.backgroundColor = .white
        self.date.backgroundColor = UIColor(named: "recordTextbox")
        datePickerSetting()
        self.yawarakasa.backgroundColor = UIColor(named: "recordTextbox")
        self.recordButton.backgroundColor = .systemGray
        self.recordButton.tintColor = .white
        self.recordButton.layer.cornerRadius = 10
        
        // textFiel の情報を受け取るための delegate を設定するとtextFieldShouldReturnとかが呼ばれる
        self.date.delegate = self
        self.yawarakasa.delegate = self
        print("レコードビューコントローラー")
        
        
        self.createMenuVC()
        self.createHeaderVC()
        
        let input = createInput()
        let output = viewModel.transform(input: input)
        setupOutput(output)
    }
    
    private func datePickerSetting() {
        // 日付ピッカー設定
        // DatePickerModeをDate(日付)に設定
        datePicker.datePickerMode = .date
        // DatePickerを日本語化
        datePicker.locale = NSLocale(localeIdentifier: "ja_JP") as Locale
        // textFieldのinputViewにdatepickerを設定
        date.inputView = datePicker
        // UIToolbarを設定
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        // Doneボタン押したらdoneClicked呼ぶ
        let doneButton = UIBarButtonItem(title: "完了", style: .done, target: nil, action: #selector(doneClicked))
        // Doneボタンを追加
        toolbar.tintColor = UIColor(named: "recordTextbox")
        toolbar.setItems([doneButton], animated: true)
        // FieldにToolbarを追加
        date.inputAccessoryView = toolbar
    }
    
    @objc func doneClicked(){
        let dateFormatter = DateFormatter()
        // 持ってくるデータのフォーマットを設定
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        dateFormatter.locale    = NSLocale(localeIdentifier: "ja_JP") as Locale?
        dateFormatter.dateStyle = DateFormatter.Style.medium
        // textFieldに選択した日付を代入
        date.text = dateFormatter.string(from: datePicker.date)
        // キーボードを閉じる
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            // キーボードを閉じる
            textField.resignFirstResponder()
            return true
    }
    
    private func createInput() -> RecordViewModel.Input {
        return RecordViewModel.Input(
            onTapRecordButton: self.recordButton.rx.tap.asSignal()
        )
    }
    
    private func setupOutput(_ output: RecordViewModel.Output) {
        output.showDialog
            .drive(onNext: { [unowned self] in
                // やわらかさ情報の登録
                recordYawarakasa()
            }).disposed(by: bag)
    }
    
    private func createMenuVC() {
        let menuVC = MenuViewController.start(self)
        addChild(menuVC)
        menuVC.didMove(toParent: self)
        menuVC.view.frame = CGRect(x: 0, y: view.bounds.size.height-120,
                                   width: view.bounds.size.width, height: 120)
        self.view.addSubview(menuVC.view)
        menuVC.recordButton.backgroundColor = UIColor(named: "selectedMenuTab")
    }
    
    private func createHeaderVC() {
        let headerVC = HeaderViewController.start(self)
        addChild(headerVC)
        headerVC.didMove(toParent: self)
        headerVC.view.frame = CGRect(x: 0, y: 0,
                                   width: view.bounds.size.width, height: 150)
        self.view.addSubview(headerVC.view)
        headerVC.view.backgroundColor = UIColor(named: "header")
        headerVC.headerText.text = "やわらかさの登録"
        headerVC.headerText.font = .boldSystemFont(ofSize: 20)
        headerVC.headerText.textColor = .white
    }
}

// MARK: - Network
extension RecordViewController {
    private func recordYawarakasa() {
        let network = NetworkLayer()

        network.setStartHandler {()->() in
            SVProgressHUD.show(withStatus: "Loading...")
        }
        network.setErrorHandler {(error: NSError)->() in
            // 通信失敗時
            SVProgressHUD.dismiss()
            print("通信に失敗しました")
        }
        network.setFinishHandler {(result: Any?)->() in
            SVProgressHUD.dismiss()
        }

        // API実行
        //TODO: nilでは空文字を弾けない
        guard let date = self.date.text else {
            // nilのときの処理
            print("日付を入力してください")
            return
        }
        guard let yawarakasa = self.yawarakasa.text else {
            // nilのときの処理
            print("やわらかさを入力してください")
            return
        }
        let separateDate:[String] = date.components(separatedBy: "/")
        let parameters = ["yawarakasa": String(yawarakasa),
                          "year": String(separateDate[0]),
                          "month": String(separateDate[1]),
                          "day": String(separateDate[2])]
        network.requestApi(api: .record,
                           parameters: parameters ,
                           headers: ["x-access-token" : UserDefaults.standard.string(forKey: "access_token")!],
                           completion: {(json) -> Void in
                            // 登録しましたのダイアログを出す
                            self.alert()
                            // テキストフィールド空にする
                           })
        print(parameters)
    }
    
    private func alert() {
        // ダイアログ出す
        let alert: UIAlertController = UIAlertController(title: "登録しました！", message: nil, preferredStyle:  UIAlertController.Style.alert)
        let confirmAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:{(action: UIAlertAction!) -> Void in
                //TODO: テキストフィールドを空にする
                print("やわらかさ登録")
            })
        alert.addAction(confirmAction)
        present(alert, animated: true, completion: nil)
    }
}
