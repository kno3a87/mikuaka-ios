//
//  RecoedViewModel.swift
//  yawacom
//
//  Created by 久野文菜 on 2021/03/29.
//

import Foundation
import RxSwift
import RxCocoa

class RecordViewModel: ViewModelType {
    private let bag = DisposeBag()
    
    func transform(input: Input) -> Output {
        let showDialog = PublishRelay<Void>()
        
        input.onTapRecordButton
            .emit(onNext: {
                showDialog.accept(())
            })
            .disposed(by: self.bag)
        
        return RecordViewModel.Output(
            showDialog: showDialog.asDriverOnErrorJustComplete()
        )
    }

    struct Input {
        let onTapRecordButton: Signal<Void>
    }

    struct Output {
        let showDialog: Driver<Void>
    }

}

