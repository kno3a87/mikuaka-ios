//
//  RegistrationViewModel.swift
//  yawacom
//
//  Created by 久野文菜 on 2021/03/28.
//

import Foundation
import RxSwift
import RxCocoa

class RegistrationViewModel: ViewModelType {
    private let bag = DisposeBag()
    
    func transform(input: Input) -> Output {
        let showRegistrationView = PublishRelay<Void>()
        
        input.onTapRegistrationButton
            .emit(onNext: {
                showRegistrationView.accept(())
            })
            .disposed(by: self.bag)
        
        return RegistrationViewModel.Output(
            showRegistrationView: showRegistrationView.asDriverOnErrorJustComplete()
        )
    }

    struct Input {
        let onTapRegistrationButton: Signal<Void>
    }

    struct Output {
        let showRegistrationView: Driver<Void>
    }

}

