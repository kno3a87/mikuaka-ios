//
//  RegistrationViewController.swift
//  yawacom
//
//  Created by 久野文菜 on 2021/01/26.
//

import Foundation
import RxSwift
import SVProgressHUD

final class RegistrationViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var sex: UISegmentedControl!
    @IBOutlet weak var registrationButton: UIButton!
    
    private var viewModel: RegistrationViewModel!
    
    static func start(
        _ currentVC: UIViewController,
        animated: Bool = false
    ) {
        let nextVC = RegistrationViewController(nibName: "RegistrationView", bundle: nil)   // .initは省略可能
        currentVC.navigationController?.pushViewController(nextVC, animated: animated)
        // 上のナビゲーションバーを非表示
        currentVC.navigationController?.navigationBar.isHidden = true
    }
    
    private let bag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewModel = RegistrationViewModel()
        
        self.view.backgroundColor = .white
        self.userName.backgroundColor = UIColor(named: "registrationTextbox")
        self.password.backgroundColor = UIColor(named: "registrationTextbox")
        self.registrationButton.backgroundColor = .systemGray
        self.registrationButton.tintColor = .white
        self.registrationButton.layer.cornerRadius = 10
        print("レジストレーションビューコントローラー")
        
        // textFiel の情報を受け取るための delegate を設定するとtextFieldShouldReturnとかが呼ばれる
        self.userName.delegate = self
        self.password.delegate = self

        let input = createInput()
        let output = viewModel.transform(input: input)
        setupOutput(output)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            // キーボードを閉じる
            textField.resignFirstResponder()
            return true
    }
    
    private func createInput() -> RegistrationViewModel.Input {
        return RegistrationViewModel.Input(
            onTapRegistrationButton: self.registrationButton.rx.tap.asSignal()
        )
    }
    
    private func setupOutput(_ output: RegistrationViewModel.Output) {
        output.showRegistrationView
            .drive(onNext: { [unowned self] in
                registerUser()
                RecordViewController.start(self)
            }).disposed(by: bag)
    }
}

// MARK: - Network
extension RegistrationViewController {
    private func registerUser() {
        let network = NetworkLayer()

        network.setStartHandler {()->() in
            SVProgressHUD.show(withStatus: "Loading...")
        }
        network.setErrorHandler {(error: NSError)->() in
            // 通信失敗時
            SVProgressHUD.dismiss()
            print("通信に失敗しました")
        }
        network.setFinishHandler {(result: Any?)->() in
            SVProgressHUD.dismiss()
        }

        // API実行
        //TODO: nilでは空文字を弾けない
        guard let userName = self.userName.text else {
            // nilのときの処理
            print("ユーザ名を入力してください")
            return
        }
        guard let password = self.password.text else {
            // nilのときの処理
            print("パスワードを入力してください")
            return
        }
        let parameters = ["name": userName, "password": password]
        network.requestApi(api: .register,
                           parameters: parameters ,
                           headers: nil,
                           completion: {(json) -> Void in
                            UserDefaults.standard.set(json["access_token"].string, forKey: "access_token")
                           })
        print(parameters)
    }
}

