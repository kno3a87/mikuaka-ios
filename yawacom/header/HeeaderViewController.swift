//
//  HeeaderViewController.swift
//  yawacom
//
//  Created by ayana.kuno on 2021/04/26.
//

import Foundation
import RxSwift

final class HeaderViewController: UIViewController {
    private var viewModel: MenuViewModel!
    @IBOutlet weak var headerText: UILabel!
    static func start(
        _ currentVC: UIViewController,
        animated: Bool = false
    ) -> HeaderViewController {
        let nextVC = HeaderViewController(nibName: "HeaderView", bundle: nil)   // .initは省略可能
        currentVC.navigationController?.pushViewController(nextVC, animated: animated)
        // 上のナビゲーションバーを非表示
        currentVC.navigationController?.navigationBar.isHidden = true
        return nextVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("ヘッダービューコントローラー")
    }
}

