//
//  ParentViewController.swift
//  yawacom
//
//  Created by 久野文菜 on 2021/04/03.
//

import Foundation
import RxSwift

final class ParentViewController: UIViewController {
    @IBOutlet weak var parentView: UIView!
    
    static func start(
        _ currentVC: UIViewController,
        animated: Bool = false
    ) {
        let nextVC = ParentViewController(nibName: "ParentView", bundle: nil)   // .initは省略可能
        currentVC.navigationController?.pushViewController(nextVC, animated: animated)
        // 上のナビゲーションバーを非表示
        currentVC.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidLoad() {
        print("ペアレントビューコントローラー")
        // 最初はやわらかさ登録ビュー表示
        RecordViewController.start(self)
    }
}
