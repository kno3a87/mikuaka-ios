//
//  LoginViewModel.swift
//  yawacom
//
//  Created by 久野文菜 on 2021/01/29.
//

import Foundation
import RxSwift
import RxCocoa

class LoginViewModel: ViewModelType {
    private let bag = DisposeBag()
    
    func transform(input: Input) -> Output {
        let showRegistrationView = PublishRelay<Void>()
        let showRecordView = PublishRelay<Void>()
        
        input.onTapRegistrationButton
            .emit(onNext: {
                showRegistrationView.accept(())
            })
            .disposed(by: self.bag)
        input.onTapLoginButton
            .emit(onNext: {
                showRecordView.accept(())
            })
            .disposed(by: self.bag)
        
        return LoginViewModel.Output(
            showRegistrationView: showRegistrationView.asDriverOnErrorJustComplete(),
            showRecordView: showRecordView.asDriverOnErrorJustComplete()
        )
    }

    struct Input {
        let onTapRegistrationButton: Signal<Void>
        let onTapLoginButton: Signal<Void>
    }

    struct Output {
        let showRegistrationView: Driver<Void>
        let showRecordView: Driver<Void>
    }

}

