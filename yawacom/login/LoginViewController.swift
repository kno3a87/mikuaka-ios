//
//  LoginViewController.swift
//  yawacom
//
//  Created by 久野文菜 on 2021/01/29.
//

import Foundation
import RxSwift
import SVProgressHUD

final class LoginViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registrationButton: UIButton!
    
    private var viewModel: LoginViewModel!  // !がないと'LoginViewController' has no initializersになる
    
    static func start(
        _ currentVC: UIViewController,
        animated: Bool = false
    ) {
        let nextVC = LoginViewController(nibName: "LoginView", bundle: nil)   // .initは省略可能
        currentVC.navigationController?.pushViewController(nextVC, animated: animated)
        // 上のナビゲーションバーを非表示
        currentVC.navigationController?.navigationBar.isHidden = true
    }

    private let bag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("ログインビューコントローラー")
        
        self.viewModel = LoginViewModel()
        
        self.view.backgroundColor = .white
        self.userName.backgroundColor = UIColor(named: "loginTextbox")
        self.password.backgroundColor = UIColor(named: "loginTextbox")
        self.loginButton.backgroundColor = .systemGray
        self.loginButton.tintColor = .white
        self.loginButton.layer.cornerRadius = 10
        
        // textFiel の情報を受け取るための delegate を設定するとtextFieldShouldReturnとかが呼ばれる
        self.userName.delegate = self
        self.password.delegate = self

        let input = createInput()
        let output = viewModel.transform(input: input)
        setupOutput(output)
        
        // トークンがあったらログイン済にしてParentViewControllerに飛ぶ
        if let _ = UserDefaults.standard.string(forKey: "access_token") {
            ParentViewController.start(self)
        }
//        print(UserDefaults.standard.string(forKey: "access_token")!)  // debug
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            // キーボードを閉じる
            textField.resignFirstResponder()
            return true
    }
    
    private func createInput() -> LoginViewModel.Input {
        return LoginViewModel.Input(
            onTapRegistrationButton: self.registrationButton.rx.tap.asSignal(),
            onTapLoginButton: self.loginButton.rx.tap.asSignal()
        )
    }
    
    private func setupOutput(_ output: LoginViewModel.Output) {
        output.showRegistrationView
            .drive(onNext: { [unowned self] in
                RegistrationViewController.start(self)
            }).disposed(by: bag)
        output.showRecordView
            .drive(onNext: { [unowned self] in
                print("login()呼ぶ")
                login()
            }).disposed(by: bag)
    }
}

// MARK: - Network
extension LoginViewController {
    private func login() {
        print("ログインだよ！")
        let network = NetworkLayer()

        network.setStartHandler {()->() in
            SVProgressHUD.show(withStatus: "Loading...")
        }
        network.setErrorHandler {(error: NSError)->() in
            // 通信失敗時
            SVProgressHUD.dismiss()
            print("通信に失敗しました")
            self.alert()
        }
        network.setFinishHandler {(result: Any?)->() in
            SVProgressHUD.dismiss()
        }

        // API実行
        //TODO: nilでは空文字を弾けない
        guard let userName = self.userName.text else {
            // nilのときの処理
            print("ユーザ名を入力してください")
            return
        }
        guard let password = self.password.text else {
            // nilのときの処理
            print("パスワードを入力してください")
            return
        }
        let parameters = ["name": userName, "password": password]
        network.requestApi(api: .login,
                           parameters: parameters ,
                           headers: nil,
                           completion: {(json) -> Void in
                            UserDefaults.standard.set(json["access_token"].string, forKey: "access_token")
                            ParentViewController.start(self)
                           })
    }
    
    private func alert() {
        //アラート生成
        //UIAlertControllerのスタイルがalert
        let alert: UIAlertController = UIAlertController(title: "ユーザ名もしくはパスワードが違います", message: "", preferredStyle:  UIAlertController.Style.alert)
        // 確定ボタンの処理
        let confirmAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:{
            // 確定ボタンが押された時の処理をクロージャ実装する
            (action: UIAlertAction!) -> Void in
            //実際の処理
            print("確定")
        })
        //UIAlertControllerにキャンセルボタンと確定ボタンをActionを追加
        alert.addAction(confirmAction)
        //実際にAlertを表示する
        present(alert, animated: true, completion: nil)
    }
}
