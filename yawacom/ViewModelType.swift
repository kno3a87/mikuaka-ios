//
//  ViewModelType.swift
//  yawacom
//
//  Created by 久野文菜 on 2021/01/29.
//

import Foundation

protocol ViewModelType {
    associatedtype Input
    associatedtype Output
    
    func transform(input: Input) -> Output
}
